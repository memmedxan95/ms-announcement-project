package az.ingress.mapper;

import az.ingress.model.Announcement;
import az.ingress.request.create.AnnouncementRequest;
import az.ingress.response.AnnouncementResponse;
import org.mapstruct.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AnnouncementMapper {

//    public static final AnnouncementMapper INSTANCE = Mappers.getMapper(AnnouncementMapper.class);

    @Mapping(target = "announcementDetail.title", source = "title" )
    @Mapping(target = "announcementDetail.description", source = "description")
    @Mapping(target = "announcementDetail.price", source = "price")
    Announcement requestToEntity(AnnouncementRequest announcementRequest);


    @Mapping(target = "title", source = "announcementDetail.title"  )
    @Mapping(target = "description", source = "announcementDetail.description")
    @Mapping(target = "price", source = "announcementDetail.price" )
    @Mapping(target = "viewCount", source = "viewCount")
    AnnouncementResponse announcementToResponse(Announcement announcement);


//    @Mapping(target = "title", source = "announcementDetail.title"  )
//    @Mapping(target = "description", source = "announcementDetail.description")
//    @Mapping(target = "price", source = "announcementDetail.price" )
//    @Mapping(target = "viewCount", source = "viewCount")
//    List<AnnouncementResponse> announcementListToResponseList(List<Announcement> announcementList);
}

