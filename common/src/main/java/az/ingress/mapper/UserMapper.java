package az.ingress.mapper;



import az.ingress.model.User;
import az.ingress.request.create.UserCreateRequest;
import az.ingress.response.UserCreateResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User createRequestToEntity (UserCreateRequest userCreateRequest);
//    User deleteRequestToEntity(UserDeleteRequest userDeleteRequest);

    UserCreateResponse entityToResponseCreate(User user);

//    UserReadResponse entityToResponseRead(User user);
//    UserUpdateResponse entityToResponseUpdate(User user);

//    User updateRequestToEntity(UserUpdateRequest userUpdateRequest);



}
