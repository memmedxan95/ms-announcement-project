package az.ingress.request.create;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementRequest {

    private String title;
    private String description;
    private double price;


}
