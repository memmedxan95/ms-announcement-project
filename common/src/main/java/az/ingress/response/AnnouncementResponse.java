package az.ingress.response;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AnnouncementResponse implements Serializable {

    private Integer viewCount;
    private String title;
    private String description;
    private double price;


}
