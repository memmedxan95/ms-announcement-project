package az.ingress.service;


import az.ingress.annotation.ConsoleLog;
import az.ingress.annotation.LogExecutionTime;
import az.ingress.constant.UserMessages;
import az.ingress.exception.AnnouncementNotFoundException;
import az.ingress.exception.UserNotFoundException;
import az.ingress.mapper.AnnouncementMapper;
import az.ingress.model.Announcement;
import az.ingress.model.User;
import az.ingress.repository.AnnouncementRepository;
import az.ingress.repository.UserRepository;
import az.ingress.request.SearchCriteria;
import az.ingress.request.create.AnnouncementRequest;
import az.ingress.response.AnnouncementResponse;
import az.ingress.specification.AnnouncementSpecification;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

import static az.ingress.constant.AnnouncementMessages.ANNOUNCEMENT_NOT_FOUND;


@Service
@RequiredArgsConstructor
@Setter
public class AnnouncementServiceImpl implements AnnouncementService {


    private final AnnouncementRepository announcementRepository;
    private final UserRepository userRepository;
    private final AnnouncementMapper announcementMapper;

    @Override
    @LogExecutionTime
    public List<Announcement> findAllAnnouncement(List<SearchCriteria> searchCriteriaList) {
        AnnouncementSpecification announcementSpecification = new AnnouncementSpecification();
        searchCriteriaList.forEach(searchCriteria -> announcementSpecification.add(searchCriteria));
        return announcementRepository.findAll(announcementSpecification);
    }

    @CachePut(value = "announcements", key = "#pageSize + '-' + #pageNumber + '-' + #pageSort")
    @LogExecutionTime
    public Page<Announcement> getAnnouncementAll(int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).descending());
        return announcementRepository.findAll(pageable);
    }


    @ConsoleLog
    @LogExecutionTime
    public void createAnnouncement(AnnouncementRequest announcementRequest, Long id) {

        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        Announcement announcement = announcementMapper.requestToEntity(announcementRequest);
        announcement.setUser(user);
        announcementRepository.save(announcement);

    }

    @Override
    @Transactional
    public void updateAnnouncement(AnnouncementRequest announcementRequest, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));

        Announcement announcement = announcementRepository.findByUser(user)
                .orElseThrow(() -> new AnnouncementNotFoundException(HttpStatus.NOT_FOUND.name(), ANNOUNCEMENT_NOT_FOUND));
        if (Objects.nonNull(announcementRequest.getTitle())) {
            announcement.getAnnouncementDetail().setTitle(announcementRequest.getTitle());
        }

        if (Objects.nonNull(announcementRequest.getDescription())) {
            announcement.getAnnouncementDetail().setDescription(announcementRequest.getDescription());
        }

        if (Objects.nonNull(announcementRequest.getPrice())) {
            announcement.getAnnouncementDetail().setPrice(announcementRequest.getPrice());
        }

        announcement.setUser(user);
        announcementRepository.save(announcement);

    }

    @Override
    @CacheEvict(value = "announcements", key = "#userId + '-' + #announcementId")
    public void deleteAnnouncement(Long announcementId, Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(
                HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        announcementRepository.findById(announcementId).orElseThrow(() ->
                new AnnouncementNotFoundException(HttpStatus.NOT_FOUND.name(), ANNOUNCEMENT_NOT_FOUND));
        announcementRepository.deleteById(announcementId);
    }

    @Override
    @CachePut(value = "announcements", key = "#id")
    public Page<Announcement> getOwnAnnouncementAll(Pageable pageable, Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(
                HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
////        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).descending());
////        Sort sort = Sort.by(pageSort).descending();
////        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
//
        return announcementRepository.findByUser(user, pageable);
    }

    @Override
//    @CachePut(value = "announcements", key = "#userId + '-' + #announcementId")
    @LogExecutionTime
    @Transactional
    public AnnouncementResponse getOwnAnnouncementById(Long userId,
                                                       Long announcementId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(
                HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        Announcement announcement = announcementRepository.findById(announcementId)
                    .orElseThrow(() -> new AnnouncementNotFoundException(
                            HttpStatus.NOT_FOUND.name(), ANNOUNCEMENT_NOT_FOUND));

            int count = announcement.getViewCount()+1;
            announcement.setViewCount(count);
            return announcementMapper.announcementToResponse(announcement);
        }

    @Override
    public Page<AnnouncementResponse> getMostViewedOwnAnnouncementById(Long userId, Pageable pageable) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(
                HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        Page <Announcement> announcement = announcementRepository.findAllByUserOrderByViewCountDesc(user, pageable)
                .orElseThrow(() -> new AnnouncementNotFoundException(
                HttpStatus.NOT_FOUND.name(), ANNOUNCEMENT_NOT_FOUND));
        return announcement.map(announcementMapper::announcementToResponse);

    }

    @Override
    public Page<AnnouncementResponse> getMostViewedAnnouncement(Pageable pageable) {
        Page <Announcement> announcement = announcementRepository.findMostByOrderDesc(pageable)
                .orElseThrow(() -> new AnnouncementNotFoundException(
                        HttpStatus.NOT_FOUND.name(), ANNOUNCEMENT_NOT_FOUND));
        return announcement.map(announcementMapper::announcementToResponse);
    }


}



