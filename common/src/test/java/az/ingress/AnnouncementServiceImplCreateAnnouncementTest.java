package az.ingress;


import az.ingress.mapper.AnnouncementMapper;
import az.ingress.model.AnnouncementDetail;
import az.ingress.request.create.AnnouncementRequest;
import az.ingress.response.AnnouncementResponse;
import az.ingress.service.AnnouncementServiceImpl;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import az.ingress.exception.AnnouncementNotFoundException;
import az.ingress.exception.UserNotFoundException;
import az.ingress.model.Announcement;
import az.ingress.model.User;
import az.ingress.repository.AnnouncementRepository;
import az.ingress.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceImplCreateAnnouncementTest {


    @Mock
    private AnnouncementRepository announcementRepository;

    @Mock
    private AnnouncementMapper announcementMapper;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AnnouncementServiceImpl announcementService;

    private User user;
    private Announcement announcement2;




    @Test
    void createAnnouncement_shouldCreateAnnouncement() {
        // Given
        Long userId = 1L;
        AnnouncementRequest announcementRequest = new AnnouncementRequest("Test Title", "Test Description", 100.0);
        User user = new User();
        user.setId(userId);
        user.setPassword("testPassword");
        user.setEmail("testEmail");
        user.setUsername( "testUser");
        Announcement announcement = announcementMapper.requestToEntity(announcementRequest);
        announcement.setUser(user);
        announcementRepository.save(announcement);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
//        when(announcementMapper.requestToEntity(announcementRequest)).thenReturn(announcement);


        announcementService.createAnnouncement(announcementRequest, userId);


        verify(announcementRepository).save(any(Announcement.class));
    }


    @Test
    void updateAnnouncement_shouldUpdateAnnouncement() {

        Long userId = 1L;
        Long announcementId = 1L;
        AnnouncementRequest announcementRequest = new AnnouncementRequest("Updated Title", "Updated Description", 200.0);
        User user = new User();
        user.setId(userId);
        user.setPassword("testPassword");
        user.setEmail("testEmail");
        user.setUsername( "testUser");
        Announcement announcement = new Announcement();
        announcement.setUser(user);
        announcement.setAnnouncementDetail(new AnnouncementDetail());
        announcement.setId(announcementId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findByUser(user)).thenReturn(Optional.of(announcement));


        announcementService.updateAnnouncement(announcementRequest, userId);


        verify(announcementRepository).save(any(Announcement.class));
        assertThat(announcement.getAnnouncementDetail().getTitle()).isEqualTo("Updated Title");
        assertThat(announcement.getAnnouncementDetail().getDescription()).isEqualTo("Updated Description");
        assertThat(announcement.getAnnouncementDetail().getPrice()).isEqualTo(200.0);
    }

    @Test
    void deleteAnnouncement_shouldDeleteAnnouncement() {
        // Given
        Long userId = 1L;
        Long announcementId = 1L;
        User user = new User();
        user.setId(userId);
        user.setPassword("testPassword");
        user.setEmail("testEmail");
        user.setUsername( "testUser");
        Announcement announcement = new Announcement();
        announcement.setUser(user);
        announcement.setAnnouncementDetail(new AnnouncementDetail());
        announcement.setId(announcementId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findById(announcementId)).thenReturn(Optional.of(announcement));

        // When
        announcementService.deleteAnnouncement(announcementId, userId);

        // Then
        verify(announcementRepository).deleteById(announcementId);
    }

    @Test
    void getOwnAnnouncementById_shouldReturnAnnouncement() {

        Long userId = 1L;
        Long announcementId = 1L;
        Long annoucementDetailId = 1L;
        User user = new User();
        user.setId(userId);
        user.setPassword("testPassword");
        user.setEmail("testEmail");
        user.setUsername("testUser");
        AnnouncementDetail announcementDetail = new AnnouncementDetail();
        announcementDetail.setTitle("Java");
        announcementDetail.setDescription("Test Description");
        announcementDetail.setPrice(100.0);
        announcementDetail.setId(annoucementDetailId);

        Announcement announcement = new Announcement();
        announcement.setUser(user);
        announcement.setAnnouncementDetail(new AnnouncementDetail());
        announcement.setId(announcementId);
        announcement.setViewCount(785);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findById(announcementId)).thenReturn(Optional.of(announcement));


        AnnouncementResponse announcementResponse = announcementService.getOwnAnnouncementById(userId, announcementId);




        assertThat(announcementResponse.getTitle()).isEqualTo(announcement.getAnnouncementDetail().getTitle());
        assertThat(announcementResponse.getDescription()).isEqualTo(announcement.getAnnouncementDetail().getDescription());
        assertThat(announcementResponse.getPrice()).isEqualTo(announcement.getAnnouncementDetail().getPrice());
    }

    @Test
    void getMostViewedAnnouncement_shouldReturnMostViewedAnnouncement() {

        int pageSize = 1;
        int pageNumber = 0;
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        List<Announcement> announcementList = Arrays.asList(
                new Announcement(1L, 100, new AnnouncementDetail(), new User()),
                new Announcement(2L, 200, new AnnouncementDetail(), new User()),
                new Announcement(3L, 300, new AnnouncementDetail(), new User())
        );
        Page<Announcement> announcementPage = new PageImpl<>(announcementList, pageable, announcementList.size());
        when(announcementRepository.findMostByOrderDesc(pageable)).thenReturn(Optional.of(announcementPage));


        Page<AnnouncementResponse> announcementResponsePage = announcementService.getMostViewedAnnouncement(pageable);


        assertThat(announcementResponsePage.getTotalElements()).isEqualTo(3);
        assertThat(announcementResponsePage.getContent().get(0).getViewCount()).isEqualTo(300);
        assertThat(announcementResponsePage.getContent().get(1).getViewCount()).isEqualTo(200);
        assertThat(announcementResponsePage.getContent().get(2).getViewCount()).isEqualTo(100);
    }



}
