package az.ingress;


import az.ingress.service.AnnouncementServiceImpl;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import az.ingress.exception.AnnouncementNotFoundException;
import az.ingress.exception.UserNotFoundException;
import az.ingress.model.Announcement;
import az.ingress.model.User;
import az.ingress.repository.AnnouncementRepository;
import az.ingress.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceImplDeleteAnnouncementTest {

    @Mock
    private AnnouncementRepository announcementRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AnnouncementServiceImpl announcementService;

    private User user;
    private Announcement announcement;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setId(1L);

        announcement = new Announcement();
        announcement.setId(1L);
        announcement.setUser(user);
    }

    @Test
    public void deleteAnnouncement_whenUserExistsAndAnnouncementExists_shouldDeleteAnnouncement() {
        when(userRepository.findById(eq(user.getId()))).thenReturn(Optional.of(user));
        when(announcementRepository.findById(eq(announcement.getId()))).thenReturn(Optional.of(announcement));

        announcementService.deleteAnnouncement(announcement.getId(), user.getId());

        verify(announcementRepository).deleteById(eq(announcement.getId()));
    }

    @Test
    public void deleteAnnouncement_whenUserDoesNotExist_shouldThrowUserNotFoundException() {
        when(userRepository.findById(eq(user.getId()))).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> announcementService.deleteAnnouncement(
                announcement.getId(), user.getId()));
    }

    @Test
    public void deleteAnnouncement_whenAnnouncementDoesNotExist_shouldThrowAnnouncementNotFoundException() {
        when(userRepository.findById(eq(user.getId()))).thenReturn(Optional.of(user));
        when(announcementRepository.findById(eq(announcement.getId()))).thenReturn(Optional.empty());

        assertThrows(AnnouncementNotFoundException.class, () -> announcementService.deleteAnnouncement(announcement.getId(), user.getId()));
    }


}
