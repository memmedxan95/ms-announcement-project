package az.ingress;


import az.ingress.exception.AnnouncementNotFoundException;
import az.ingress.exception.UserNotFoundException;
import az.ingress.model.Announcement;
import az.ingress.model.AnnouncementDetail;
import az.ingress.model.User;
import az.ingress.repository.AnnouncementRepository;
import az.ingress.repository.UserRepository;
import az.ingress.response.AnnouncementResponse;
import az.ingress.service.AnnouncementServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.Collections;
import java.util.Optional;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceImplGetMostViewedAnnouncementTest {

    @Mock
    private AnnouncementRepository announcementRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AnnouncementServiceImpl announcementService;

    @Test
    void testGetMostViewedOwnAnnouncementById_UserAndAnnouncementExist_ViewCounts() {
        Long userId = 1L;
        Long announcementId = 1L;
        User user = new User();
        user.setId(userId);
//        user.setPassword("testPassword");
//        user.setEmail("testEmail");
//        user.setUsername( "testUser");
        Announcement announcement = new Announcement();
        announcement.setUser(user);
        announcement.setAnnouncementDetail(new AnnouncementDetail());
        announcement.setId(announcementId);
        announcement.setViewCount(10);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findAllByUserOrderByViewCountDesc(user, (any(Pageable.class))))
                .thenReturn(Optional.of(new PageImpl<>(Collections.singletonList(announcement))));

        Page<AnnouncementResponse> result = announcementService.getMostViewedOwnAnnouncementById(userId, PageRequest.of(0, 10));


        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent().get(0).getViewCount()).isEqualTo(10);
    }


    @Test
    void testGetMostViewedOwnAnnouncementById_UserExists_NoAnnouncement() {

        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findAllByUserOrderByViewCountDesc(user, any(Pageable.class)))
                .thenReturn(Optional.empty());


        assertThrows(AnnouncementNotFoundException.class, () ->
                announcementService.getMostViewedOwnAnnouncementById(userId, PageRequest.of(0, 10)));
    }

    @Test
    void testGetMostViewedOwnAnnouncementById_UserDoesNotExist() {

        Long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());


        assertThrows(UserNotFoundException.class, () ->
                announcementService.getMostViewedOwnAnnouncementById(userId, PageRequest.of(0, 10)));
    }

    @Test
    void testGetMostViewedOwnAnnouncementById_UserAndAnnouncementExist_NoViewCounts() {

        Long userId = 1L;
        Long announcementId = 1L;
        User user = new User();
        user.setId(userId);
        Announcement announcement = new Announcement();
        announcement.setId(announcementId);
        announcement.setUser(user);
        announcement.setViewCount(0);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findAllByUserOrderByViewCountDesc(user, any(Pageable.class)))
                .thenReturn(Optional.of(new PageImpl<>(Collections.singletonList(announcement))));


        Page<AnnouncementResponse> result = announcementService.getMostViewedOwnAnnouncementById(userId, PageRequest.of(0, 10));


        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent().get(0).getViewCount()).isEqualTo(0);

    }

    @Test
    void testGetMostViewedOwnAnnouncementById_UserExists_NoAnnouncements() {

        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findAllByUserOrderByViewCountDesc(user, any(Pageable.class)))
                .thenReturn(Optional.of(new PageImpl<>(Collections.emptyList())));

        Page<AnnouncementResponse> result = announcementService.getMostViewedOwnAnnouncementById(userId, PageRequest.of(0, 10));

        assertThat(result.getTotalElements()).isEqualTo(0);
    }

}
