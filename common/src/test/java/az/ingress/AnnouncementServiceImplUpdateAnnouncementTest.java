package az.ingress;


import az.ingress.model.AnnouncementDetail;
import az.ingress.request.create.AnnouncementRequest;
import az.ingress.service.AnnouncementServiceImpl;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import az.ingress.exception.AnnouncementNotFoundException;
import az.ingress.exception.UserNotFoundException;
import az.ingress.model.Announcement;
import az.ingress.model.User;
import az.ingress.repository.AnnouncementRepository;
import az.ingress.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AnnouncementServiceImplUpdateAnnouncementTest {

    @Mock
    private AnnouncementRepository announcementRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AnnouncementServiceImpl announcementService;

    @Test
    void updateAnnouncement_throwsException_whenPriceIsNegative_withNonExistingUser() {

        Long userId = 1L;
        Long announcementId = 1L;
        AnnouncementRequest announcementRequest = new AnnouncementRequest();
        announcementRequest.setPrice(-100.0);


        when(userRepository.findById(userId)).thenReturn(Optional.empty());


        assertThrows(UserNotFoundException.class, () -> {

            announcementService.updateAnnouncement(announcementRequest, userId);
        });
    }

    @Test
    void updateAnnouncement_throwsException_whenPriceIsNegative_withNonExistingAnnouncement() {

        Long userId = 1L;
        Long announcementId = 1L;
        AnnouncementRequest announcementRequest = new AnnouncementRequest();
        announcementRequest.setPrice(-100.0);


        User user = new User();
        user.setId(userId);


        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findByUser(user)).thenReturn(Optional.empty());


        assertThrows(AnnouncementNotFoundException.class, () -> {

            announcementService.updateAnnouncement(announcementRequest, userId);
        });
    }

    @Test
    void updateAnnouncement_doesNotThrowException_whenPriceIsPositive() {

        Long userId = 1L;
        Long announcementId = 1L;
        AnnouncementRequest announcementRequest = new AnnouncementRequest();
        announcementRequest.setPrice(100.00);


        User user = new User();
        user.setId(userId);
        AnnouncementDetail announcementDetail = new AnnouncementDetail();

        Announcement announcement = new Announcement();
        announcement.setId(announcementId);
        announcement.setUser(user);
        announcement.setAnnouncementDetail(announcementDetail);


        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(announcementRepository.findByUser(user)).thenReturn(Optional.of(announcement));


        assertDoesNotThrow(() -> {

            announcementService.updateAnnouncement(announcementRequest, userId);
        });
    }







}
