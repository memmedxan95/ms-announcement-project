package az.ingress.controller;


import az.ingress.request.create.AuthenticateRequest;
import az.ingress.request.create.JwtDto;
import az.ingress.request.create.UserCreateRequest;
import az.ingress.request.login.UserLoginRequest;
import az.ingress.response.UserCreateResponse;
import az.ingress.service.UserCreateServiceImpl;
import az.ingress.service.UserLoginService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/users")
public class UserController {


    private final UserLoginService userLoginService;
    private final UserCreateServiceImpl userCreateServiceImpl;



    @PostMapping("/registration")
    @ResponseStatus(CREATED)
    public ResponseEntity<UserCreateResponse> userRegistration(@RequestBody UserCreateRequest userCreateRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(userCreateServiceImpl.createUser(userCreateRequest));
    }

    @PostMapping("/verification")
    public ResponseEntity<Void> activate(@RequestParam("email") String email,
                                         @RequestParam("verificationCode") Integer verificationCode) {
        userCreateServiceImpl.activate(email, verificationCode);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/resendVerificationCode")
    public ResponseEntity<Void> resendVerificationCode(@RequestParam("email") String email) {
        userCreateServiceImpl.resendVerificationCode(email);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/authenticate")
    public ResponseEntity<JwtDto> authenticate(@Valid @RequestBody AuthenticateRequest authenticateRequest) {
        return ResponseEntity.ok().body(userCreateServiceImpl.authenticate(authenticateRequest));
    }




    @GetMapping("/login")
//    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<String> login(@RequestBody UserLoginRequest userLoginRequest) {
        return ResponseEntity.ok(userLoginService.login(userLoginRequest));
    }

//    @PutMapping("/verify/{id}")
//    @PreAuthorize("#principal.username == 'xan'")
//    public ResponseEntity<String> verify(@PathVariable Long id) {
//        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("xan", "123"));
//        return ResponseEntity.ok().body(userCreateService.activeUser(id));
//    }



}
