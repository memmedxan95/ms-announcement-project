package az.ingress.exception;

public class AccountNumberIsSame extends  RuntimeException{

    public AccountNumberIsSame(String code, String message) {
        super(message);
    }

}
