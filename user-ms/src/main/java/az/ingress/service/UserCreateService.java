package az.ingress.service;


import az.ingress.request.create.AuthenticateRequest;
import az.ingress.request.create.JwtDto;
import az.ingress.request.create.UserCreateRequest;
import az.ingress.response.UserCreateResponse;

public interface UserCreateService {


    UserCreateResponse createUser(UserCreateRequest userCreateRequest);

    void activate(String email, Integer verificationCode);

    void resendVerificationCode(String email);

    JwtDto authenticate(AuthenticateRequest authenticateRequest);

}
